package com.example.wheelshare1;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
	public final static String EXTRA_MESSAGE = "com.example.wheelshare1.MESSAGE";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void authenticate(View view) {
	    	Intent intent = new Intent(this, HomeActivity.class);
	    	EditText editText = (EditText) findViewById(R.id.emailAddress);
	    	String  email = editText.getText().toString();
	    	//now must access data base and verify this email is registered.
	    	editText = (EditText) findViewById(R.id.password);
	    	String  password = editText.getText().toString();
	    	if(password == "password")//authenticate password if user is found
	    		password = "password";
	    	intent.putExtra(EXTRA_MESSAGE, email);
	    	startActivity(intent);
	}
}
